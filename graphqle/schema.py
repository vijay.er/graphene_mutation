import graphene
from graphene_django.debug import DjangoDebug
import tutorial.schema


class Query(
    tutorial.schema.Query,
    graphene.ObjectType,

):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(
    tutorial.schema.QuestionMutation,
    tutorial.schema.AnswerMutation,
    graphene.ObjectType,
):
    debug = graphene.Field(DjangoDebug, name="_debug")


schema = graphene.Schema(query=Query, mutation=Mutation)
