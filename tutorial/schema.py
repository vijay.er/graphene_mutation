from .models import *
from graphene import Node
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
import graphene


class QuestionNode(DjangoObjectType):
    class Meta:
        model = Question
        interfaces = (Node,)
        filter_fields = ["question"]


class AnswerNode(DjangoObjectType):
    class Meta:
        model = Answer
        interfaces = (Node,)
        filter_fields = ["answer", "question"]


class CreateQuestion(graphene.Mutation):
    id = graphene.Int()
    question = graphene.String()

    class Arguments:
        question = graphene.String()

    def mutate(self, info, question):
        question = Question(question=question)
        question.save()

        return CreateQuestion(
            id=question.id,
            question=question.question,

        )


class CreateAnswer(graphene.Mutation):
    answer = graphene.String()
    question = graphene.String()

    class Arguments:
        answer = graphene.String()
        question = graphene.String()

    def mutate(self, info, answer, question):
        intans = Question.objects.get(question=question)
        answer = Answer(answer=answer,
                        question=intans)
        answer.save()

        return CreateAnswer(
            answer=answer.answer,
            question=answer.question,

        )


class AnswerMutation(object):
    create_answer = CreateAnswer.Field()


class QuestionMutation(object):
    create_question = CreateQuestion.Field()


class Query(object):
    question = Node.Field(QuestionNode)
    all_question = DjangoFilterConnectionField(QuestionNode)

    answer = Node.Field(AnswerNode)
    all_answer = DjangoFilterConnectionField(AnswerNode)
